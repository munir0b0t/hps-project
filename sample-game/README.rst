===========
Sample Game
===========

The sample game is quite similar to the Auction game. The clients send a number
to the server and the server determines the max number. The games continues until
one client has won 3 times. This is meant to highlight the usage of the both the
classes of client-server pairs. It can be used as a starting point for desiging
the games.

To run the game, you will need to install the package first. After that, just run
``python max_bid_socket.py`` or ``python max_bid_zmq.py`` to see the results.
