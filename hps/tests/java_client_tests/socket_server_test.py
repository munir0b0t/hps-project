import sys
import os
from time import sleep

__this_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(__this_dir + '/../../')

from servers import SocketServer
from clients.python.socket_client import SocketClient

HOST = '127.0.0.1'
PORT = 9000

server = SocketServer(HOST, PORT, 1)
server.establish_client_connections()
server.send_to_all('Hello')
print("Server has sent Hello")
resp = server.receive_from(0)
print("Server has received " + resp)

server.close()

