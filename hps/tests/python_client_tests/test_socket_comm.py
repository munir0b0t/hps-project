import os
import sys
import unittest

from utils import start_server


__this_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(__this_dir + '/../../')

from servers import SocketServer
from clients.python.socket_client import SocketClient


class test_socket_comm(unittest.TestCase):

    HOST = '127.0.0.1'
    PORT = 9000

    def test_vanilla_client(self):

        def vanilla_server():
            server = SocketServer(self.HOST, self.PORT, 1)
            server.establish_client_connections()
            server.send_to_all('Hello')
            resp = server.receive_from(0)
            self.assertEqual(resp, 'World')
            server.close()

        server = start_server(vanilla_server)
        client = SocketClient(self.HOST, self.PORT)
        data = client.receive_data()
        self.assertEqual(data, 'Hello')
        client.send_data('World')
        client.close()
        server.terminate()

    def test_many_clients(self):

        def many_server():
            server = SocketServer(self.HOST, self.PORT, 3)
            server.establish_client_connections()
            server.send_to_all('Who are you?')
            resps = [server.receive_from(i) for i in range(0, 3)]
            self.assertEqual(resps[0], 'Client 0')
            self.assertEqual(resps[0], 'Client 1')
            self.assertEqual(resps[0], 'Client 2')
            server.close()

        server = start_server(many_server)
        clients = []
        for i in range(0, 3):
            clients.append(SocketClient(self.HOST, self.PORT))
        data = [client.receive_data() for client in clients]
        for datum in data:
            self.assertEqual(datum, 'Who are you?')
        for i in range(0, 3):
            clients[i].send_data('Client %d' % i)
        for c in clients:
            c.close()
        server.terminate()


if __name__ == '__main__':
    unittest.main(exit=False)
