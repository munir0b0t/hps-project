import sys
import os
from time import sleep

__this_dir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(__this_dir + '/../../')

from servers import ZmqServer
from clients.python.zmq_client import ZmqClient

HOST = '127.0.0.1'
SEND_PORT = 9000
RECV_PORT = 9001

server = ZmqServer(HOST, SEND_PORT, RECV_PORT, 1)
server.establish_client_connections()
server.send_to_all('Hello')
print("Server has sent Hello")
resp = server.receive_from_all()
print("Server has received " + resp[0]['data'])

server.close()
