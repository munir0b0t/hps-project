#include "./../../clients/cplusplus/zmq_client.h"
#include <iostream>
#include <unistd.h>

int main(int argc, char const *argv[]) {
    ZmqClient test_client = ZmqClient("127.0.0.1", 9000, 9001, "test_client");
    cout << "Client has connected to server" << endl;
    string sent_from_server = test_client.receive_data();
    cout << "Client has received " << sent_from_server << endl;
    test_client.send_data("World");
    cout << "Client has sent World" << endl;
    sleep((unsigned int)1);
    test_client.close_zmq_client();
    cout << "Client has closed socket" << endl;
}
