#include "./../../clients/cplusplus/socket_client.h"
#include <iostream>

int main(int argc, char const *argv[]) {
    cout << "Testing vanilla client" << endl;
    string HOST = "127.0.0.1";
    int PORT = 9000;
    SocketClient test_client = SocketClient(HOST,PORT);
    cout << "Client has connected to server" << endl;
    string sent_from_server = test_client.receive_data(4096);
    cout << "Client has received " << sent_from_server << endl;
    test_client.send_data("World");
    cout << "Client has sent World" << endl;
    test_client.close_socket();
    cout << "Client has closed socket" << endl << endl;
    
    cout << "Testing that receive large can receive anything" << endl;
    SocketClient test_client3 = SocketClient(HOST, PORT);
    cout << "Client has connected to server" << endl;
    sent_from_server = test_client3.receive_large(4096,1);
    cout << "Client has received " << sent_from_server << endl;
    test_client3.close_socket();
    cout << "Client has closed socket" << endl << endl;
    
    cout << "Testing timeout in receive large" << endl;
    SocketClient test_client2 = SocketClient(HOST, PORT);
    cout << "Client has connected to server" << endl;
    sent_from_server = test_client2.receive_large(4096,2);
    cout << "Exited after timeout" << endl;
    test_client2.close_socket();
    cout << "Client has closed socket" << endl;
}
