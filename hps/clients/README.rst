+++++++
Clients
+++++++

Currently, the library contains Python 3, C++, and Java clients for the servers
provided. Clients in various languages implement similar interfaces but
there might be language specific quirks in the implementations. For
example, statically typed languages cannot implement interfaces with
dynamic types. It is best to consult the documentation of the particular
client you are using.

Installing
----------

Python 3
========

The Python 3 clients are installed as part of this package. You can use
them in your code by importing them as ``from hps.clients import <ClientClass>``.
You will need ZeroMQ installed to use the ``ZmqClient``.

C++
===

For the ``SocketClient``, simply include the ``socket_client.hpp`` file in
your code.

For the ``ZmqClient``, follow the steps below:
    1. Install ZeroMQ from your package manager or through one of the methods at
       `ZeroMQ download page <https://zeromq.org/intro:get-the-software>`_ or
        by compiling for the `git repo <https://github.com/zeromq/cppzmq>`_.
    2. Include the header file for the client in your main code.

Java
====

For the ``SocketClient``, just copy the client source code into your project's
repo or source code directory.

For the ``ZmqClient``, follow the steps below:
    1. Install `jzmq <https://github.com/zeromq/jzmq>`_.
    2. Copy the client file into your source code directory.
    3. Compile the client with ``javac -cp /usr/local/share/java/zmq.jar *.java``
    4. To run the code, execute ``java -cp .:/usr/local/share/java/zmq.jar -Djava.library.path=/usr/local/lib main``
