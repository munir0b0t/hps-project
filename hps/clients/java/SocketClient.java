/**
 * A socket based client for the ``hps.servers.SocketServer`` class
 */

import java.awt.*;
import java.io.*;
import java.net.*;

public class SocketClient {
    /**
     * A client class for ``hps.servers.SocketServer``
     */
    public String host;
    public int port;
    public Socket socket;

    public SocketClient(String host, int port) throws IOException {
        /**
         *   @param host: The hostname of the server
         *   @param port: The port of the server
         */
        this.host = host;
        this.port = port;
        InetAddress inetAddress = InetAddress.getByName(host);
        SocketAddress socketAddress = new InetSocketAddress(inetAddress, port);
        socket = new Socket();
        socket.connect(socketAddress);
    }

    public void send_data(String data) throws IOException {
        /**
         * Send data to the server
         *
         * @param data: The data to send to the server.
         */
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        bufferedWriter.write(data);
        bufferedWriter.flush();
    }

    public String receive_data() throws IOException {
        /**
         * Receive data from the server
         *
         * @return The data received as a String from server.
         */
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        StringBuffer sb = new StringBuffer();
        String str;
        while (bufferedReader.ready()) {
            sb.append((char) bufferedReader.read());
        }
        return sb.toString();
    }

    public void close_socket() throws IOException {
        /**
         * Close the connection
         */
        socket.close();
    }
}


