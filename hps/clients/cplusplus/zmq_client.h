#include <stdio.h>
#include <string>
#include <sstream>

#include <zmq.hpp>

using namespace std;

/*
 * A ZeroMQ based client for the ``hps.servers.ZmqServer`` class
 */
class ZmqClient {
public:
    string token;
    zmq::socket_t *req_socket;
    zmq::socket_t *pull_socket;
    zmq::context_t *client_context;
    ZmqClient(string, int, int, string);
    ~ZmqClient();
    void close_zmq_client();
    string receive_data();
    string send_data(string);
};

/*
 *  ZmqClient Constructor
 *  Arguments:
 *  string host_name:
 *      the host name of the server
 *  int server_send_port:
 *      the port from which the server sends data. This will be the
 *      port on which the client receives data.
 *  int server_recv_port:
 *      the port on which the server receives data. This will be the
 *      port to which the client will send data.
 *  string client_id:
 *      an identifier for the client which has to be unique for all
 *      clients connected to a single server.
 */
ZmqClient::ZmqClient(string host_name, int server_send_port, int server_recv_port, string client_id) {
    zmq::context_t * context = new zmq::context_t(1);
    zmq::socket_t * socket1 = new zmq::socket_t(*context, ZMQ_PULL);
    zmq::socket_t * socket2 = new zmq::socket_t(*context, ZMQ_REQ);

    stringstream ss;
    ss << "tcp://" << host_name << ":" << server_send_port;
    string connect_string = ss.str();

    socket1->connect(connect_string);
    this->pull_socket = socket1;

    ss.clear();
    ss.str(string());
    connect_string.clear();

    ss << "tcp://" << host_name << ":" << server_recv_port;
    connect_string = ss.str();

    socket2->connect (connect_string);

    zmq::message_t client_id_message((void*)client_id.c_str(), client_id.length(), NULL);
    socket2->send(client_id_message);
    zmq::message_t reply;
    socket2->recv(&reply);

    this->token = string((char*)reply.data(),reply.size());
    this->req_socket = socket2;

    this->client_context = context;
}

/*
 *  ZmqClient Send Data
 *  The client will send the data to the server until it gets a 200
 *  response from the server. If the server does not respond with a
 *  202 or 200 status, an error is raised.
 *  Arguments:
 *      string data:
 *          the data to send
 *  Return:
 *      the reply from server as a string
 */
string ZmqClient::send_data(string data) {
    string to_send = this->token + " " + data;
    zmq::message_t request((void*)to_send.c_str(), to_send.length(), NULL);
    string resp = "202";
    string received;
    zmq::message_t reply;

    while (resp.find("200") != 0) {
        received.clear();

        req_socket->send(request);
        req_socket->recv(&reply);

        received = string((char *) reply.data(), reply.size());
        resp = received.substr(0, received.find(" "));

        if (resp.find("200") != 0 && resp.find("202") != 0)
            throw "Expected 200 or 202 response. Server responded with " + resp + ".";
    }
    received.erase(received.begin(), received.begin() + 3);
    return received;
}

/*
 *  ZmqClient Receive Data
 *  Return:
 *      a string sent by the server
 */
string ZmqClient::receive_data() {
    zmq::message_t reply;
    pull_socket->recv(&reply);
    return string((char *) reply.data(), reply.size());
}

/*
 *  ZmqClient Close Socket
 */
void ZmqClient::close_zmq_client() {
    req_socket->close();
    pull_socket->close();
    zmq_ctx_destroy(client_context);
}

/*
 *  ZmqClient Destructor
 */
ZmqClient::~ZmqClient() {
    close_zmq_client();
}
