==========
Server API
==========

This document describes the API implemented by the servers provided
in this package.

If the method is preceeded by a class name, only that particular class
implements the method. If there is no class name, the method is generic
and the documentation will mention and class specific caveats.

There are two classes of servers available, a ``SocketServer`` and a
``ZmqServer``. The ``SocketServer`` class is a TCP socket based server
and the ``ZmqServer`` is a `ZeroMQ <https://zreomq.org/>`_ based server.

The ``SocketServer`` is mainly intended for use in games where the
communication is not done in parallel and the server talks to only one
client at a time. It should not be used in cases where the server is
expected to send or receive data from multiple clients in parallel. Most
games satisfy this constraints.

The ``ZmqServer`` is intended for use in games where the communication
is done in parallel and the server will receive data from multiple
clients concurrently. It also supports 1 to 1 communication and can be
used for other games as well with a some small changes to how the clients
and server communicate.

See the `Communication Design doc <./design.rst>`_ for details on how to
setup the client and server communication for both the servers.

Constructors, destructors and other methods
-------------------------------------------

**SocketServer(host, port, num_clients)**:

The server socket is created as ``self.sck`` attribute of the class and the
client sockets are saved in the ``self.client_sockets`` list in the order in
which they connect to the server after ``establish_client_connections`` is
called.

Args:
    :host: The hostname or IP address on which the server
        should run
    :port: The port on which the server should listen
    :num_clients: The number of clients the server will connect to


**ZmqServer(send_port, recv_port, num_clients)**:

The connected clients are saved as ``self.client_ids`` dictionary after the
``establish_client_connections`` method is called. The keys are the client
ids provided by the clients and the value is a uuid that serves as a cookie
for the client.

Args:
    :host:
        The host name of the server
    :send_port:
        The port from which the server sends data. This will
        be the port on which the client receives data.
    :recv_port:
        The port on which the server receives data. This will
        be the port to which the client will send data.
    :num_clients:
        The number of clients that the server will communicate with

**close(self)**:

Clean-up method for the server. This will be called automatically
when the program exits, but can be called manually to free up the
resources used by the server if it is not required anymore.

**establish_client_connections(self)**:

This method establishes connections with the clients. In most cases,
this will be called immediately after the server is setup. This method
must be called before any kind of communication can be done using
the server.

Methods to send data
--------------------

The method signatures described here are for generic signatures
that are implemented by all servers. Classes may have a other
keyword arguments, but calling the methods with these signatures
should work on all server classes.

| **send_to(self, data, client_idx)**:
| **send_to_all(self, data)**:

These are the two main methods implemented by the servers for sending
data. The ``send_to`` is used to send data to a particular client
while the ``send_to_all`` method sends data to all connected clients.
The ``client_idx`` in the ``send_to`` method identifies the client
and is implementation dependant. ``data`` must be a string.

Note:
    See the docstrings of the methods within the individual classes for
    implementation specific details.

Args:
    :data (str): The data to send to the client(s)
    :client_idx: Implementation specific identifier for individual
        clients

**send_file(self, filename, client_idx=None)**:

The ``send_file`` method is a special method to send the contents of
a file to one or all clients. If ``client_idx`` is ``None``, the data
is send to all clients.

Args:
    :filename (str): The filename relative to the working directory
        of the server or an absolute path.
    :client_idx: Implementation specific identifier for individual
        clients. If ``None``, the contents of the file are sent to
        all connected clients.

Methods to receive data
-----------------------

As with the send methods, the signatures here are generic signatures
that should work on all servers, but individual classes may implement
more keyword arguments than described here.

| **receive_from(self, client_idx, size=4096)**:
| **receive_from_until(self, client_idx, condition, size=4096)**:
| **receive_from_all(self, size=4096)**:

The ``receive_from`` and ``receive_from_until`` methods receive data from
a specific client specified by ``client_idx`` while the ``receive_from_all``
method receives data from all clients. The ``client_idx`` is implementation
dependent. The ``condition`` is a function that takes the data received from
server as an argument and returns a Boolean, which causes the method
to request more data if ``False`` and stop receiving if ``True``. The order
in which ``receive_from_all`` receives the data is implementation dependent.

Note:
    The ``receive_from_until`` method is not supported by the ``ZmqServer``
    since ZeroMQ handles the size of data received internally.

Args:
    :client_idx: Implementation specific identifier for individual clients.
    :condition: A function that takes a single argument, the data received
        so far, and returns a Boolean based on that.
    :size: The number of bytes to receive in a single system call. Default
        is 4096.

Return:
    The ``SocketServer`` returns the data received as a string in case of
    ``receive_from`` and ``receive_from_until`` and a list of strings in
    case of ``receive_from_all``.

    The ``ZmqServer`` returns the data received as string in case of
    ``receive_from`` and the a list of dictionaries in case of
    ``receive_from_all``. The list is sorted in order in which the data
    was received. The dictionaries contain the keys ``time``,
    ``client_id`` and ``data`` where ``time`` is the ``datetime.datetime``
    object representing the time at which the data was received, the
    ``client_id`` is the id of the client which sent the data and ``data``
    is a string of the message received.
