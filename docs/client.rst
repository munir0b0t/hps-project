==========
Client API
==========

This document describes the API implemented by the clients provided
in this package.

Since the clients are implemented in multiple languages, there is no
consistent API due to the differences in the nature of the languages.
For example, Python allows default values for arguments but Java does
not. You should refer to the docstrings of the client of the language
you are using for the most accurate documentation of a particular method.
This document will broadly describe what methods are available.

There are two client classes available in all languages, ``SocketClient``
and ``ZmqClient``. The ``SocketClient`` is meant for use with the
``SocketServer`` and the ``ZmqClient`` is meant for use with the
``ZmqServer``. The clients can only send and recieve data from one server
at a time and do not support multiple servers.

See the `Communication Design doc <./design.rst>`_ for details on how to
setup the client and server communication.

Constructors, destructors and other methods
-------------------------------------------

**SocketClient(host, port)**:

Args:
    :host: The hostname or IP address of the server to connect to
    :port: The port of the server to connect to

**ZmqClient(host, server_send_port, server_recv_port, client_id)**:

Args:
    :host: The hostname or IP address of the server to connect to
    :server_send_port: The port from which the server will send data
    :server_recv_port: The port on which the server will receive data
    :client_id: An idenitfier for the client, which must be unique
        for all clients connected to the server. Usually, this will be
        the team name.

**close()**:

Clean-up method for the client. This should be called before exiting
the program for a clean exit. If the language supports it, this will
be called by the client's destructor or during the exit process. It

Methods to send data
--------------------

The method signatures here are generic signatures, which may not be
supported by all languages. Check the documentation of the particular
client that you are using for exact usage details.

**send_data(data)**:
**send_file(file_name)**:

Sends the ``data`` or the contents of ``file_name`` to the server.

Note:
    ``send_file`` is not supported by all platforms and not implemented
    by the ``ZmqClient``

Args:
    :data (str): A string to send to the server
    :file_name (str): A file name whose contents will be sent to
        the server.


Methods to receive data
-----------------------

As with the send methods, the signatures here are generic and the
docstrings of the particular client you are using should be consulted
for exact details.

**receive_data(size)**:

Recieve ``size`` bytes of data from the server. This is simplest
way to receive data and should be the first choice for receiving data.
Depending on the language, there may be restrictions on how large
``size`` can be, but anything upto ``102400`` (100 kb) should be
easily supported.

Args:
    :size: Number of bytes to receive. This may be a default value
        if the language supports it.

Return:
    The data received as a string.

**receive_large(chunk_size, timeout)**:

Receive a large amount of data from the server. This is quite useful
for receiving the contents of a file from the server. The method will
work for all sizes of data but the performance is not good for small
data sizes. Usually, you will want to use ``receive_until`` if the
client supports it instead of this method.

This method receives chunks of data from the server until the timeout
has expired without receiving any data from the server. It returns
all

Note:
    This method is not implemented in Java since large receives are
    handled internally by Java

    This method is not implemented by the ``ZmqClient`` since ZeroMQ
    takes care of the data size issues internally.

Args:
    :chunk_size: The number of bytes of each individual chunk of
        data received from the server
    :timeout: The time to wait before which the data received from
        the server is considered complete

**receive_until(condition, size)**:

Receives data until the received data satisfies a condition. Not all
languages implement this method since it requires passing a function
as an argument. For those languages, you can use ``receive_large``
instead.

The method will receive data and call the ``condition`` function on
it. If the function returns ``False``, more data is received and checked.
This is done iteratively until the function returns ``True``. If
supported, this should be the primary method to receive a large size
of data where the condition for end of data is known.

Note:
    This method can block execution if the condition is never
    satisfied. It is advisable to set a timeout on the socket before
    calling this method to prevent that.

    This method is not implemented in Java in since large receives
    are handled internally by Java.

    This method is not implemented in C++.

    This method is not implemented by the ``ZmqClient`` since ZeroMQ
    takes care of the data size issues internally.

Args:
    :condition: A function which takes one argument, the data
        received so far, and returns a Boolean
    :size: The number of bytes of data to receive in each iteration
